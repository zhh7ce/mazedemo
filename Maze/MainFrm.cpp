// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Maze.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

static UINT uHideCmds[] =
{
	ID_FILE_PRINT,
	ID_FILE_PRINT_PREVIEW,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	XTPPaintManager()->SetTheme(xtpThemeRibbon);

	if (!CreateStatusBar())
		return -1;

	if (!InitCommandBars())
		return -1;

	CXTPCommandBars* pCommandBars = GetCommandBars();
	m_wndStatusBar.SetCommandBars(pCommandBars);

	if (!CreateRibbonBar())
	{
		TRACE0("Failed to create ribbon\n");
		return -1;
	}

	CXTPToolTipContext* pToolTipContext = GetCommandBars()->GetToolTipContext();
	pToolTipContext->SetStyle(xtpToolTipResource);
	pToolTipContext->ShowTitleAndDescription();
	pToolTipContext->ShowImage(TRUE, 0);
	pToolTipContext->SetMargin(CRect(2, 2, 2, 2));
	pToolTipContext->SetMaxTipWidth(180);
	pToolTipContext->SetFont(pCommandBars->GetPaintManager()->GetIconFont());
	pToolTipContext->SetDelayTime(TTDT_INITIAL, 900);

	pCommandBars->GetCommandBarsOptions()->bShowKeyboardTips = TRUE;

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;
	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers




void CMainFrame::OnClose()
{

	// Save the current state for toolbars and menus.
	SaveCommandBars(_T("CommandBars"));


	CFrameWnd::OnClose();
}

BOOL CMainFrame::CreateStatusBar()
{
	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return FALSE;      // fail to create
	}

	m_wndStatusBar.AddIndicator(0);
	m_wndStatusBar.AddIndicator(ID_INDICATOR_CAPS);
	m_wndStatusBar.AddIndicator(ID_INDICATOR_NUM);
	m_wndStatusBar.AddIndicator(ID_INDICATOR_SCRL);

	return TRUE;
}

BOOL CMainFrame::CreateRibbonBar()
{
	CXTPCommandBars* pCommandBars = GetCommandBars();

	CMenu menu;
	menu.Attach(::GetMenu(m_hWnd));
	SetMenu(NULL);

	CXTPRibbonBar* pRibbonBar = (CXTPRibbonBar*)pCommandBars->Add(_T("The Ribbon"), xtpBarTop, RUNTIME_CLASS(CXTPRibbonBar));
	if (!pRibbonBar)
	{
		return FALSE;
	}

	pRibbonBar->EnableDocking(0);

	CXTPControlPopup* pControlFile = (CXTPControlPopup*)pRibbonBar->AddSystemButton(ID_MENU_FILE);
	pControlFile->SetCommandBar(menu.GetSubMenu(0));
	pControlFile->GetCommandBar()->SetIconSize(CSize(32, 32));
	pCommandBars->GetImageManager()->SetIcons(ID_MENU_FILE);
	pControlFile->SetCaption(_T("&File"));
	
	//pControlFile->SetIconId(IDB_GEAR);
	//UINT uCommand = {IDB_GEAR};
	//pCommandBars->GetImageManager()->SetIcons(IDB_GEAR, &uCommand, 1, CSize(0, 0), xtpImageNormal);
	
	CXTPRibbonTab* pTabHome = pRibbonBar->AddTab(ID_TAB_BUTTONS);

	// Large Buttons
	if (pTabHome)
	{
		CXTPControl* pControl;

		CXTPRibbonGroup* pGroup = pTabHome->AddGroup(ID_GROUP_LARGEBUTTONS);
		pGroup->ShowOptionButton();

		pControl = pGroup->Add(xtpControlButton, ID_FILE_OPEN);
		pControl->SetStyle(xtpButtonIconAndCaptionBelow);

		pControl = pGroup->Add(xtpControlButton, ID_FILE_SAVE);
		pControl->SetStyle(xtpButtonIconAndCaptionBelow);

		pControl = pGroup->Add(xtpControlButton, ID_MAZE_CREATE);
		pControl->SetStyle(xtpButtonIconAndCaptionBelow);

		pControl = pGroup->Add(xtpControlButton, ID_MAZE_FIND);
		pControl->SetStyle(xtpButtonIconAndCaptionBelow);

		//UINT nIDs[] = {ID_BUTTON_LARGESIMPLEBUTTON, ID_BUTTON_LARGEPOPUPBUTTON, ID_BUTTON_LARGESPLITPOPUPBUTTON, ID_BUTTON_LARGETOGGLEBUTTON};
		//pCommandBars->GetImageManager()->SetIcons(ID_GROUP_LARGEBUTTONS, nIDs, 4, CSize(32, 32), xtpImageNormal);
	}

	//if (pTabHome)
	//{
	//	CXTPRibbonGroup* pGroup = pTabHome->AddGroup(ID_GROUP_GROUP);
	//	pGroup->SetControlsGrouping(TRUE);
	//	pGroup->SetControlsCentering(TRUE);

	//	pGroup->LoadToolBar(IDR_MAINFRAME);
	//}

	// Options
	//{
	//	CXTPControlPopup* pControlOptions = (CXTPControlPopup*)pRibbonBar->GetControls()->Add(xtpControlPopup, -1);
	//	pControlOptions->SetFlags(xtpFlagRightAlign);
	//	CMenu mnuOptions;
	//	mnuOptions.LoadMenu(IDR_MENU_OPTIONS);
	//	pControlOptions->SetCommandBar(mnuOptions.GetSubMenu(0));
	//	pControlOptions->SetCaption(_T("Options"));
	//	
	//	
	//	CXTPControl* pControlAbout = pRibbonBar->GetControls()->Add(xtpControlButton, ID_APP_ABOUT);
	//	pControlAbout->SetFlags(xtpFlagRightAlign);

	//	pCommandBars->GetImageManager()->SetIcons(IDR_MAINFRAME);	

	//}

	// Quick Access
	{

		pRibbonBar->GetQuickAccessControls()->Add(xtpControlButton, ID_FILE_NEW);
		pRibbonBar->GetQuickAccessControls()->Add(xtpControlButton, ID_FILE_OPEN)->SetHideFlag(xtpHideCustomize, TRUE);
		pRibbonBar->GetQuickAccessControls()->Add(xtpControlButton, ID_FILE_SAVE);
		
		pRibbonBar->GetQuickAccessControls()->Add(xtpControlButton, ID_FILE_PRINT);
		pRibbonBar->GetQuickAccessControls()->CreateOriginalControls();

	}

	pRibbonBar->SetCloseable(FALSE);
	pRibbonBar->EnableFrameTheme();

	return TRUE;
}