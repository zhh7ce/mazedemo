// Maze.h : main header file for the Maze application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CMazeApp:
// See Maze.cpp for the implementation of this class
//

class CMazeApp : public CWinApp
{
public:
	CMazeApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CMazeApp theApp;