// MazeDoc.cpp : implementation of the CMazeDoc class
//

#include "stdafx.h"
#include "Maze.h"

#include "MazeDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMazeDoc

IMPLEMENT_DYNCREATE(CMazeDoc, CDocument)

BEGIN_MESSAGE_MAP(CMazeDoc, CDocument)
END_MESSAGE_MAP()


// CMazeDoc construction/destruction

CMazeDoc::CMazeDoc()
{
	// TODO: add one-time construction code here
	m = new Maze(30, 30);
	m->Initialize();

}

CMazeDoc::~CMazeDoc()
{
}

BOOL CMazeDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CMazeDoc serialization

void CMazeDoc::Serialize(CArchive& ar)
{
	m->Serialize(ar);
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CMazeDoc diagnostics

#ifdef _DEBUG
void CMazeDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMazeDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMazeDoc commands
