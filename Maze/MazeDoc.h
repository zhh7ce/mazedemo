// MazeDoc.h : interface of the CMazeDoc class
//


#pragma once
#include "myMaze.h"

class CMazeDoc : public CDocument
{
protected: // create from serialization only
	CMazeDoc();
	DECLARE_DYNCREATE(CMazeDoc)

// Attributes
public:
	Maze *m;

// Operations
public:

// Overrides
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CMazeDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};
