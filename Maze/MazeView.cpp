// MazeView.cpp : implementation of the CMazeView class
//

#include "stdafx.h"
#include "Maze.h"

#include "MazeDoc.h"
#include "MazeView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMazeView

IMPLEMENT_DYNCREATE(CMazeView, CView)

BEGIN_MESSAGE_MAP(CMazeView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_COMMAND(ID_MAZE_CREATE, &CMazeView::OnMazeCreate)
	ON_COMMAND(ID_MAZE_FIND, &CMazeView::OnMazeFind)
END_MESSAGE_MAP()

// CMazeView construction/destruction

CMazeView::CMazeView()
{
	// TODO: add construction code here

}

CMazeView::~CMazeView()
{
}

BOOL CMazeView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMazeView drawing

void CMazeView::OnDraw(CDC* /*pDC*/)
{
	CMazeDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
	OpenGLRender();
}


// CMazeView printing

BOOL CMazeView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMazeView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMazeView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CMazeView diagnostics

#ifdef _DEBUG
void CMazeView::AssertValid() const
{
	CView::AssertValid();
}

void CMazeView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMazeDoc* CMazeView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMazeDoc)));
	return (CMazeDoc*)m_pDocument;
}
#endif //_DEBUG


// CMazeView message handlers


bool CMazeView::setPixelFormat()
{
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR), // 结构的大小  
		1, // 结构的版本  
		PFD_DRAW_TO_WINDOW | // 在窗口(而不是位图)中绘图  
		PFD_SUPPORT_OPENGL | // 支持在窗口中进行OpenGL调用  
		PFD_DOUBLEBUFFER, // 双缓冲模式  
		PFD_TYPE_RGBA, // RGBA颜色模式  
		32, // 需要32位颜色  
		0, 0, 0, 0, 0, 0, // 不用于选择模式  
		0, 0, // 不用于选择模式  
		0, 0, 0, 0, 0, // 不用于选择模式  
		16, // 深度缓冲区的大小  
		0, // 在此不使用  
		0, // 在此不使用  
		0, // 在此不使用  
		0, // 在此不使用  
		0, 0, 0 // 在此不使用  
	};
	// 选择一种与pfd所描述的最匹配的像素格式  
	// 为设备环境设置像素格式  
	int pixelformat;
	pixelformat = ChoosePixelFormat(m_pDC->GetSafeHdc(), &pfd);
	if (0 == pixelformat) return false;
	// 为设备环境设置像素格式  
	return SetPixelFormat(m_pDC->GetSafeHdc(), pixelformat, &pfd);
	return false;
}


int CMazeView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码
	// 创建DC  

	m_pDC = new CClientDC(this);
	ASSERT(m_pDC != NULL);
	// 选择像素格式  
	if (!setPixelFormat()) return -1;
	// 创建渲染环境, 并使它成为当前渲染环境  
	m_hRC = wglCreateContext(m_pDC->GetSafeHdc());
	wglMakeCurrent(m_pDC->GetSafeHdc(), m_hRC);
	OpenGLRender();
	return 0;
}


void CMazeView::OnDestroy()
{
	CView::OnDestroy();

	// TODO:  在此处添加消息处理程序代码
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(m_hRC);
	delete m_pDC;
}


void CMazeView::OnInitialUpdate()
{
	CView::OnInitialUpdate();
	// TODO:  在此添加专用代码和/或调用基类
	glClearColor(1, 1, 1, 1);
	OpenGLRender();
}


void CMazeView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	// TODO:  在此处添加消息处理程序代码
	//glViewport(0, 0, cx, cy);
	//// 设置投影矩阵(透视投影)  
	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	//gluPerspective(60.0, (GLfloat)cx / (GLfloat)cy, 1.0, 1000.0);
	//// 设置模型视图矩阵  
	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	//gluLookAt(0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	// 显示三维图形的程序
	/*glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();*/

	GLfloat nRange = 20.0f;

	if (cy == 0)
		cy = 1;

	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (cx <= cy)
		glOrtho(-nRange, nRange, -nRange*cy / cx, nRange*cy / cx, -nRange, nRange);
	else
		glOrtho(-nRange*cx / cy, nRange*cx / cy, -nRange, nRange, -nRange, nRange);

	glPushMatrix();

	OpenGLRender();
}


void CMazeView::OpenGLRender()
{
	// TODO: 在此添加命令处理程序代码
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glMatrixMode(GL_MODELVIEW);
	//glPushMatrix();

	//glColor3f(1.0,0.0,0.0);
	//glLineWidth(3);
	//if (m != nullptr) m->Show();
	//
	//glPopMatrix();
	//// 交换缓冲区  
	//SwapBuffers(wglGetCurrentDC());
	GetDocument()->m->Show();
}


void CMazeView::OnMazeCreate()
{
	// TODO: 在此添加命令处理程序代码
	GetDocument()->m = new Maze(30,30);
	GetDocument()->m->Initialize();
	GetDocument()->m->DFSGenerate();
	//m->DFSFind(0,0);
	OpenGLRender();
}


BOOL CMazeView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if (WM_KEYFIRST <= pMsg->message && pMsg->message <= WM_KEYLAST) {
		switch (pMsg->wParam) {
		case VK_UP: GetDocument()->m->Move(2); break;
		case VK_DOWN: GetDocument()->m->Move(8); break;
		case VK_LEFT: GetDocument()->m->Move(4); break;
		case VK_RIGHT: GetDocument()->m->Move(1); break;
		default: break;
		}
	}
	
	OpenGLRender();

	return CView::PreTranslateMessage(pMsg);
}


void CMazeView::OnMazeFind()
{
	// TODO: 在此添加命令处理程序代码
	GetDocument()->m->DFSFind(GetDocument()->m->m_x, GetDocument()->m->m_y);
	CDialog* dlg = new CDialog();
	dlg->Create(IDD_ABOUTBOX, this);
	dlg->ShowWindow(SW_NORMAL);
}
