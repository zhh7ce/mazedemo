// MazeView.h : interface of the CMazeView class
//


#pragma once

class CMazeView : public CView
{
protected: // create from serialization only
	CMazeView();
	DECLARE_DYNCREATE(CMazeView)

// Attributes
public:
	CMazeDoc* GetDocument() const;

// Operations
public:

// Overrides
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CMazeView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

private:
    HGLRC m_hRC;
    CClientDC* m_pDC;

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	bool setPixelFormat();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	void OpenGLRender();
	afx_msg void OnMazeCreate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnMazeFind();
};

#ifndef _DEBUG  // debug version in MazeView.cpp
inline CMazeDoc* CMazeView::GetDocument() const
   { return reinterpret_cast<CMazeDoc*>(m_pDocument); }
#endif
