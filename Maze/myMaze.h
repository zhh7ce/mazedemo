class Maze :
	public CObject {
   private:
    /* data */
    int **ptr;
    int row, col;
	bool showProcess;

   public:
	DECLARE_SERIAL(Maze)
	int m_x, m_y;
	Maze();
    Maze(int row, int col);
    ~Maze();
    void Initialize();
    void DFSGenerate();
	bool DFSFind(int x, int y);
    void Show();
	void Move(int m);
	virtual void Serialize(CArchive& ar);
};
