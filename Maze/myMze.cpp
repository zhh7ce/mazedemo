#include "stdafx.h"
#include "myMaze.h"

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>

#include "malloc.h"
#include "windows.h"


using std::cout;
using std::endl;
using std::rand;
using std::srand;
using std::vector;

#define MASK 15

IMPLEMENT_SERIAL(Maze, CObject, 1)

Maze::Maze() {
	int row = 30; col = 30;
    ptr = (int**)malloc(sizeof(int*) * row);
    for (int i = 0; i < row; ++i) {
        ptr[i] = (int*)malloc(sizeof(int) * col);
    }
    this->row = row;
    this->col = col;
	showProcess = true;
}

Maze::Maze(int row, int col) {
    ptr = (int**)malloc(sizeof(int*) * row);
    for (int i = 0; i < row; ++i) {
        ptr[i] = (int*)malloc(sizeof(int) * col);
    }
    this->row = row;
    this->col = col;
	showProcess = true;
}

Maze::~Maze() {
    for (int i = 0; i < row; ++i) {
        free(ptr[i]);
    }
    free(ptr);
}

void Maze::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{	// storing code
		ar << m_x;
		ar << m_y;
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				ar << ptr[i][j];
			}
		}
	}
	else
	{	// loading code
		ar >> m_x;
		ar >> m_y;
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				ar >> ptr[i][j];
			}
		}
	}
}


void Maze::Initialize() {
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            ptr[i][j] = MASK;
        }
    }
    ptr[0][0] &= MASK - 8;
	m_x = 0;
	m_y = 0;
	Show();
}

void Maze::DFSGenerate() {
    srand(time(0));
    vector<int> stackX;
    vector<int> stackY;
    stackX.push_back(0);
    stackY.push_back(0);
    int x, y;
    int length;
    int arr[4];
    while (!stackX.empty()) {
        x = stackX.back();
        y = stackY.back();
        stackX.pop_back();
        stackY.pop_back();
        while (true) {
            length = 0;
            if (y != 0 && ptr[x][y - 1] == MASK) {
                arr[length] = 0;
                length++;
            }
            if (x != 0 && ptr[x - 1][y] == MASK) {
                arr[length] = 1;
                length++;
            }
            if (y != col - 1 && ptr[x][y + 1] == MASK) {
                arr[length] = 2;
                length++;
            }
            if (x != row - 1 && ptr[x + 1][y] == MASK) {
                arr[length] = 3;
                length++;
            }

            if (length == 0) break;

            switch (arr[rand() % length]) {
                case 0:
                    ptr[x][y] &= MASK - 8;
                    stackX.push_back(x);
                    stackY.push_back(y);
                    y--;
                    ptr[x][y] &= MASK - 2;
                    break;
                case 1:
                    ptr[x][y] &= MASK - 4;
                    stackX.push_back(x);
                    stackY.push_back(y);
                    x--;
                    ptr[x][y] &= MASK - 1;
                    break;
                case 2:
                    ptr[x][y] &= MASK - 2;
                    stackX.push_back(x);
                    stackY.push_back(y);
                    y++;
                    ptr[x][y] &= MASK - 8;
                    break;
                case 3:
                    ptr[x][y] &= MASK - 1;
                    stackX.push_back(x);
                    stackY.push_back(y);
                    x++;
                    ptr[x][y] &= MASK - 4;
                    break;

                default:
                    break;
            }
			if (showProcess) Show();
        }
    }
    ptr[row - 1][col - 1] &= MASK - 2;
	ptr[0][0] |= 32;
	ptr[0][0] |= 16;
}

bool Maze::DFSFind(int x, int y) {
	ptr[x][y] |= 32;
	ptr[x][y] |= 16;
	if (showProcess) {
		Show();
		Sleep(10);
	}
	if (x == row-1 && y == col-1) {
		return true;
	}
    if (!(ptr[x][y] & 2) && !(ptr[x][y+1] & 16)) {
		if (DFSFind(x, y+1)) return true;
    }
    if (!(ptr[x][y] & 1) && !(ptr[x+1][y] & 16)) {
		if (DFSFind(x+1, y)) return true;
    }
    if (!(ptr[x][y] & 4) && !(ptr[x-1][y] & 16)) {
		if (DFSFind(x-1, y)) return true;
    }
    if (!(ptr[x][y] & 8) && !(ptr[x][y-1] & 16)) {
		if (DFSFind(x, y-1)) return true;
    }
	ptr[x][y] &= 255 - 32;
	return false;
}

void Maze::Show() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glColor3f(1.0,0.0,0.0);
	glLineWidth(3);

	GLfloat stride = 1;
	GLfloat offset = -15;
	GLfloat x, y;
	glBegin(GL_LINES);
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
			x = i * stride + offset;
			y = j * stride + offset;
			if (ptr[i][j] & 8) {
				glVertex2f(x, y);
				glVertex2f(x+stride, y);
			}
			if (ptr[i][j] & 4) {
				glVertex2f(x, y);
				glVertex2f(x, y+stride);
			}
			if (ptr[i][j] & 2) {
				glVertex2f(x, y+stride);
				glVertex2f(x+stride, y+stride);
			}
			if (ptr[i][j] & 1) {
				glVertex2f(x+stride, y);
				glVertex2f(x+stride, y+stride);
			}
        }
    }
	glEnd();

	glColor3f(0.0,1.0,0.0);
	glLineWidth(3);

    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
			x = i * stride + offset;
			y = j * stride + offset;
			if (ptr[i][j] & 32) {
				glRectf(x+stride/4, y+stride/4, x+3*stride/4, y+3*stride/4);
			}
        }
    }

		
	glPopMatrix();
	// ����������  
	SwapBuffers(wglGetCurrentDC());
}

void Maze::Move(int m) {
	if (ptr[m_x][m_y] & m) return;
	int dx = 0, dy = 0;
	switch (m) {
		case 8: dy--; break;
		case 4: dx--; break;
		case 2: dy++; break;
		case 1: dx++; break;
		default: break;
	}
	if (ptr[m_x+dx][m_y+dy] >= 32) {
		ptr[m_x][m_y] &= 255 - 32;
		ptr[m_x][m_y] &= 255 - 16;
	}
	m_x += dx;
	m_y += dy;
	ptr[m_x][m_y] |= 32;
	ptr[m_x][m_y] |= 16;
}

