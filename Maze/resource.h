//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Maze.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define ID_MENU_FILE                    101
#define ID_TAB_BUTTONS                  102
#define ID_GROUP_LARGEBUTTONS           103
#define IDR_MAINFRAME                   128
#define IDR_MazeTYPE                    129
#define IDB_BITMAP_ICONS                130
#define IDR_MAZE                        131
#define IDR_PANE_OPTIONS                4000
#define IDR_PANE_PROPERTIES             4001
#define ID_MAZE_32771                   32771
#define ID_MAZE_CREATE                  32772
#define ID_MAZE_CREATEW                 32773
#define ID_MAZE_32774                   32774
#define ID_MAZE_FIND                    32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
